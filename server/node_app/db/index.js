var mysql=require("mysql");

//We are Pooling Connections
db_conn=mysql.createPool({
  connectionLimit:4,
  host:"localhost",
  user:"pusher",
  password:"abc123",
  database:"pusher",
  port:3306,
  supportBigNumbers :true
});

process.on("SIGINT", function() {
    db_conn.end();
    process.exit();
});
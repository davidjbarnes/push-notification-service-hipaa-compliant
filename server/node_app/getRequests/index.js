var legalGets = require("./legalGets.js");
var getRequests = function (req, res, next) {
    if(legalGets.indexOf(req.params[0]) != -1) { 
        var obj=req.params[0].split("/")[0];
        var method=req.params[0].split("/")[1];
        
        switch(obj) {
            case "token":
                var TokenMgmt=require("../token");
                var token=new TokenMgmt();
                token[method](req, res, next);
                break;
            case "notification":
                var NotificationMgmt=require("../notification");
                var notifiction=new NotificationMgmt();
                notifiction[method](req, res, next);
                break;
            default:
                BadMethod(res, next)
            break;
            }
    } else {
        BadMethod(res, next)
    }
}

module.exports = getRequests;
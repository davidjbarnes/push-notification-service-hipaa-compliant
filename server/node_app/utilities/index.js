SuccessResponse = function(res,next,sendData){ 
    res.send(200,{
        status:"Success",
        data:sendData
    })
    next();
}
HackDetected = function(res,next){ res.send(401,{status: "Hacker", token:"" });}
BadMethod =function (res,next){res.send(405, {status: "Bad Method" }); next();}
NotAuthorized = function(res,next){res.send(401, 
                                            {status: "Not Authorized",
                                             data:{   
                                                    authenticated: false 
                                                }
                                            }); next();}
NotAuthenticated = function(res,next){
    res.send(401,{status: "Not Authorized",
                  data:{   
                      authenticated: false 
                  }
                 }); 
    next();
}
ServerError = function(res,next){res.send(500, {status: "Server Error"}); next();}

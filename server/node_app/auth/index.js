var AuthMgmt = function () {
	this.sql_strings = require("./query.js");
	this.fs = require("fs");
	this.q = require("q");
	this.ruleEngine = require("node-rules");
	this.rules = [];
}

UserMgmt.prototype.getuser = function (req, res, next){
        db_conn.query(this.sql_strings.getUsers, [], function(err, rows, fields){
            if(err){
                ServerError(res, next);
                throw  err
            }
            
            if(rows.length >= 0){
                SuccessResponse(res, next, {users:rows});
            }
        })

}

module.exports = UserMgmt;
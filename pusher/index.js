var http = require("http");
var request = require("request");
var async=require("async");

module.exports = {
    apiKey:null,
    token:null,
    pollMs:3000,

    init: function(options) {
        var self=this;
        this.apiKey=options.apiKey;
        this.getToken(function (response) {
            self.token=response.data.token;
            options.onRegister(response.data);
            //Start polling...
            self.poll(options.onNotification);
        });
    },
    getToken: function (callback) {
        var apiKey=this.apiKey;
        request("http://localhost:5050/token/get/"+apiKey, function (error, response, body) {
            callback(JSON.parse(body));
        });
    },
    poll: function (callback) {
        var token=this.token;
        var pollMs=this.pollMs;
        console.info("Waiting for new notifications... TOKEN: ", token);
        setInterval(function(){
            request("http://localhost:5050/notification/get/"+token, function (error, response, body) {
                callback(JSON.parse(body));
            }); 
        }, pollMs);
    },
    confirm: function (messageGUID) {

    },
    info: function () {
        return this;
    },
};














// async.parallel({
        //     one: function(callback) {
        //         apiKey=apiKey;
        //         callback(null, apiKey);
        //     },
        //     two: function(callback) {
        //         request("http://localhost:5050/api/token/asdf", function (error, response, body) {
        //             token=JSON.parse(body).token;
        //             //callback(JSON.parse(body).token);
        //             //console.log(JSON.parse(body).token);
        //             callback(null, token);
        //         });
        //     }
        // }, function(err, results) {
        //     // results is now equals to: {one: 1, two: 2}
        //     //console.info(results);

        //     callback(results);
        // });






// return {
//             "getToken":function (apiKey) {
                
//                     return request("http://localhost:5050/api/token/"+apiKey, function (error, response, body) {
//                         if (!error && response.statusCode == 200) {
//                             console.log(JSON.parse(body).token);
//                         }
//                     });
                
//             },
//             "apiKey":apiKey,
//             "info":function () {
//                 console.info(apiKey);
//             }
//         };